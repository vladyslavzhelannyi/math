package test.java.com.math.services;

import main.java.com.math.services.MathTasksService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

public class MathTasksServiceTest {
    MathTasksService cut = new MathTasksService();

    static Arguments[] getFlightDistanceDegreeTestArgs(){
        return new Arguments[]{
            Arguments.arguments(3.2 , 57.17, 0.95137, 0.00001),
            Arguments.arguments(4.1 , 45.2, 1.7141, 0.00001)
        };
    }

    static Arguments[] getFlightDistanceRadTestArgs(){
        return new Arguments[]{
                Arguments.arguments(3.2, 1.0, 0.94947, 0.00001),
                Arguments.arguments(4.1, 0.6, 1.59765, 0.00001)
        };
    }

    static Arguments[] getDistanceBtwCarsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(5.0, 7.0, 18.0, 3.0, 54.0, 0.00001),
                Arguments.arguments(6.7, 2.3, 12.9, 1.9, 30.0, 0.00001)
        };
    }

    static Arguments[] findOutIfPointInFigureTestArgs(){
        return new Arguments[]{
                Arguments.arguments(2.0, 2.0, 1),
                Arguments.arguments(-4.3, 2.2, 0),
                Arguments.arguments(0.5, 0.1, 1),
                Arguments.arguments(0.5, 1.5, 0)
        };
    }

    static Arguments[] getValueOfExpressionTask4Args(){
        return new Arguments[]{
                Arguments.arguments(4.7, 6.00786, 0.00001),
                Arguments.arguments(5.8, 8.65171, 0.00001)
        };
    }

    @ParameterizedTest
    @MethodSource("getFlightDistanceDegreeTestArgs")
    void getFlightDistanceDegreeTest(double startingSpeed, double angle, double expected, double error){
        double actual = cut.getFlightDistanceDegree(startingSpeed, angle);
        Assertions.assertEquals(expected, actual, error);
    }

    @ParameterizedTest
    @MethodSource("getFlightDistanceRadTestArgs")
    void getFlightDistanceRadTest(double startingSpeed, double angle, double expected, double error){
        double actual = cut.getFlightDistanceRad(startingSpeed, angle);
        Assertions.assertEquals(expected, actual, error);
    }

    @ParameterizedTest
    @MethodSource("getDistanceBtwCarsTestArgs")
    void getDistanceBtwCarsTest(double speedCar1, double speedCar2, double initialDistance, double time,
                                double expected, double error){
        double actual = cut.getDistanceBtwCars(speedCar1, speedCar2, initialDistance, time);
        Assertions.assertEquals(expected, actual, error);
    }

    @ParameterizedTest
    @MethodSource("findOutIfPointInFigureTestArgs")
    void findOutIfPointInFigureTest(double coordinateX, double coordinateY, int expected){
        double actual = cut.findOutIfPointInFigure(coordinateX, coordinateY);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("getValueOfExpressionTask4Args")
    void getValueOfExpressionTask4Test(double x, double expected, double error){
        double actual = cut.getValueOfExpressionTask4(x);
        Assertions.assertEquals(expected, actual, error);
    }
}
